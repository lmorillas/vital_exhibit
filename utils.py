# coding: utf-8

coles = '''CEIP Alpartir
CRA Sahún
IES Pirámide
IES Salvador Victoria
IES Gúdar - Javalambre
CEIP Virgen del portal (Maella)
CRA L'albada (Bujaraloz)
CIFE María de Ávila
CEIP Movera
CPIFP Los Enlaces
IES Tiempos Modernos
CEIP Cortes de Aragón
CEIP Ramón Sainz de Varanda
CEIP Castillo Qadrit
CEIP Pedro Sánchez Ciruelo
CEIP San Roque (María de Huerva)
IES Miguel Servet
IES Luis Buñuel
CEIP Ciudad de Zaragoza
IES Corona de Aragon
CRA Albardín
IES Medina Albaida
CEIP Cesar Augusto
CEIP Jeronimo Blancas
CEIP Emilio Díaz
CEIP Fernández Vizarra
CEIP ZAlfonada
CEIP Tubalcain
CEIP Miguel Artazos
CEIP Sanchez Vizarra
CEIP Florencio Jardiel
CEIP Parque Europa
CEIP Lucien Briet
CEIP La Estrella
CRA Bajo Gallego (perdiguera)
IES Maria Moliner
Ceip Cervantes (Pedrola)
CEIP Utebo 4
Ceip Vadorrey
IES Miguel Servet
CRA Insula Barataria'''.split('\n')


from geopy import geocoders

gc = geocoders.GoogleV3()


def localiza(c):
    l = gc.geocode(c + ', Spain')
    if l:
    	return l
    l = gc.geocode(c + ', Zaragoza, Spain')
    if l:
    	return l
    	l = gc.geocode(c + ', Teruel, Spain')
    if l:
    	return l
    l = gc.geocode(c + ', Huesca, Spain')
    return l


pos = [localiza(c) for c in coles]



for c, p in zip(coles, pos):
    print c, p and "; {}, {}".format(p.latitude, p.longitude)


lcoles = []
for c, p in zip(coles, pos):
    lcoles.append(dict(zip(('nombre', 'latlon'), (c, "{}, {}".format(p.latitude, p.longitude) if p else ""))))


import json
json.dump(lcoles, open('vital.json', 'w'), indent=True)
